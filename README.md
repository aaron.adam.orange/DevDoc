[Gamma Ray 軟體工作室](https://gamma-ray-studio.github.io/zh-cht/index.html)
======
`歡迎來到我的軟體開發工作室`

### 這裡是一個程式開發者的[技術工作坊](https://www.youtube.com/user/rhxs020) !

**我會在這邊:**

+ 開發程式
+ 研究技術
+ 思考人生

```
不是補習班、也不是工程師頻道。 
```

**是技術流的軟體工程師，炫技與展示才華的地方!**


<br>

【工作室名稱】
------
### 取自「 Gamma Ray Burst 」`(伽瑪射線爆)`

起源於，在軟體工程師的職務上，對於各種不嚴謹工序的不滿，導致憤怒的想要爆炸 !


<br>

【諮詢服務】
------
`工作室提供 Java、Python、Web、Android、iOS ... 等，程式技術的諮詢服務`

如果在課業上的專題、論文，或者是工作上的專案、產品，遇到不熟悉、不知道怎麼實作的問題

**這邊提供一對一的付費諮詢 !**

+ [諮詢服務 - FAQ](https://gamma-ray-studio.github.io/zh-cht/consult-faq.html)


<br>

【我是誰 ? 】
------
### 我是 Richard，精通孫子兵法，擅長紙上談兵 !

喜歡物聯網跨平台整合的東東，有用  Linkit Smart 7688 搭了個小系統自娛自樂，
能開發 Java 後端、Web 前端，也能撰寫行動應用 Android 與 iOS App，

    目標是朝擁有十八般武藝的「架構師」前進，有朝一日打造自己的 iOT 系統。


<br>

【更新時間 ?】
------
就跟預估「開發時程」一樣，保留一點適當且模糊的空間 (非常的不肯定)

    「理論上」應該是 禮拜五 或 禮拜六 的晚上，可能會有新的影片

(題目如果太困難 或者 遇到連假、過年過節，可能就會怠惰，畢竟「休息是為了走更長遠的路」)


<br>

【訂閱頻道 ! 】
------
+ 點個「喜歡」👍   可以讓我知道有幫助到你～
+ 點個「訂閱」👀   則可以讓我知道，有觀眾想要看到更多的內容 ~

數位筆記還有更多的內容可以分享，並且還在不斷增加，這裡會一步一步的整理出來。

    如果你已經訂閱了，感謝你 ! 身為作者，覺得非常開心😀


<br>

Link
------
### Youtube
<https://www.youtube.com/user/rhxs020>

### Instagram
<https://www.instagram.com/gammaray_studio/>

### Facebook
<https://www.facebook.com/gammaray.studio>

### Blogger
<https://gamma-ray-studio.blogspot.com/>

### GitLab
<https://gitlab.com/GammaRayStudio/DevDoc>


